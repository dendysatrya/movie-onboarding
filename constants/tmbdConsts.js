import tmdb from "../tmdb.json";

const baseUrl = "https://api.themoviedb.org/3/";
const discoverUrl = "movie/popular";
const movieUrl = "movie/";
const recommendation = "/recommendations";
const authApi = `?api_key=${tmdb.base.aKey}`;
const sortByReleaseDate = "&sort_by=release_date.desc";
const pagination = "&page=";
const imagePath = "https://image.tmdb.org/t/p/w400";
const imagePathSmall = "https://image.tmdb.org/t/p/w200";

export {
  baseUrl,
  discoverUrl,
  movieUrl,
  authApi,
  recommendation,
  sortByReleaseDate,
  pagination,
  imagePath,
  imagePathSmall,
};
