import { Col, Row, Table, Tooltip, Typography } from "antd";
import nookies from "nookies";
import DashboardLayout from "../components/dashboardLayout";
import firebaseAdmin from "../utils/firebaseAdmin";
import { db } from "../utils/firebaseClient";
import styles from "../styles/Dashboard.module.scss";
import {
  RightOutlined,
  LeftOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import { useState } from "react";
import { getMovieDetail } from "../utils/fetchQuery";
import MovieCardMini from "../components/movieCardMini";

export default function Dashboard({ userData, userList }) {
  const [expandId, setExpandId] = useState(null);
  const [showLovedMovie, setShowLovedMovie] = useState(false);
  const [loadingMovie, setLoadingMovie] = useState(false);
  const [lovedMovie, setLovedMovie] = useState([]);
  const [nameDetail, setNameDetail] = useState(null);

  const columnTableData = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Loved movie",
      dataIndex: "lovedMovie",
      align: "center",
      key: "lovedMovie",
      render: (text, row) => {
        return (
          <Tooltip placement="bottom" title="Click to see user's loved movie.">
            <RightOutlined
              className={styles.expandIcon}
              onClick={() => {
                handleShow(row.uid, row.name);
              }}
            />
          </Tooltip>
        );
      },
    },
    {
      title: "Role",
      dataIndex: "role",
      key: "role",
    },
  ];

  const handleShow = async (uid, name) => {
    setLoadingMovie(true);
    setShowLovedMovie(true);
    setExpandId(uid);
    setNameDetail(name);
    const lovedMovies = await db
      .collection("liked_movie")
      .where("uid", "==", `${uid}`)
      .get();
    const lovedMovieArray = lovedMovies.docs.map((doc) => doc.data().movie_id);
    const movieDetailData = await Promise.all(
      lovedMovieArray.map(async (idMovie) => {
        const singleMovie = await getMovieDetail({ id: idMovie });
        return {
          id: singleMovie.id,
          title: singleMovie.title,
          posterPath: singleMovie.poster_path,
        };
      })
    );
    setLovedMovie(movieDetailData);
    setLoadingMovie(false);
  };

  const handleBack = () => {
    setExpandId(null);
    setShowLovedMovie(false);
    setLovedMovie([]);
    setLoadingMovie(false);
  };

  return (
    <DashboardLayout user={userData}>
      <div
        className={styles.userDashboardContainer}
        style={{
          transform: showLovedMovie ? "translateX(-50%)" : "translateX(0)",
        }}
      >
        <div className={styles.userTable}>
          <Typography.Title className={styles.mainText}>
            User management
          </Typography.Title>
          <Table columns={columnTableData} dataSource={userList} />
        </div>
        <div className={styles.userLovedMovie}>
          <div className={styles.backButton} onClick={handleBack}>
            <LeftOutlined className={styles.backIcon} />
            <Typography.Text className={styles.backText}>Back</Typography.Text>
          </div>
          <div className={styles.lovedMovieContainer}>
            <Typography.Title className={styles.userName}>
              {nameDetail}'s loved movie
            </Typography.Title>
            {loadingMovie ? (
              <LoadingOutlined className={styles.loadingIcon} />
            ) : (
              <div className={styles.lovedMovieListContainer}>
                {lovedMovie.length ? (
                  <Row gutter={5}>
                    {lovedMovie.map((movie) => {
                      return (
                        <Col span={4} key={movie.id}>
                          <MovieCardMini
                            title={movie.title}
                            id={movie.id}
                            posterPath={movie.posterPath}
                          />
                        </Col>
                      );
                    })}
                  </Row>
                ) : (
                  <Typography.Text className={styles.noLovedMovie}>
                    Currently this user still doesn't loved any movie.
                  </Typography.Text>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    </DashboardLayout>
  );
}

export async function getServerSideProps(ctx) {
  const cookies = nookies.get(ctx);
  let uid = "";
  let userData = null;
  if (cookies.token) {
    const token = await firebaseAdmin.auth().verifyIdToken(cookies.token);
    uid = token.uid;
    userData = {
      ...(await db.collection("users").doc(uid).get()).data(),
      uid: uid,
    };

    if (userData.role !== "admin") {
      ctx.res.writeHead(302, { Location: "/" });
      ctx.res.end();

      return { props: {} };
    }

    const usersRef = (await db.collection("users").get()).docs;
    const userList = usersRef.map((doc) => {
      return { ...doc.data(), uid: doc.id, key: doc.id };
    });

    return { props: { userData, userList } };
  } else {
    ctx.res.writeHead(302, { Location: "/" });
    ctx.res.end();

    return { props: {} };
  }
}
