import React, { useEffect, useState } from "react";
import BaseLayout from "../../components/baseLayout";
import { useRouter } from "next/router";
import { getMovieDetail, getRecommedation } from "../../utils/fetchQuery";
import { Col, Image, Layout, Row, Typography } from "antd";
import { imagePath, imagePathSmall } from "../../constants/tmbdConsts";
import { HeartOutlined, LinkOutlined, HeartFilled } from "@ant-design/icons";
import Link from "next/link";
import Moment from "react-moment";
import MovieCardMini from "../../components/movieCardMini";
import { db } from "../../utils/firebaseClient";
import nookies from "nookies";
import firebaseAdmin from "../../utils/firebaseAdmin";
import styles from "../../styles/DetailPage.module.scss";

export default function MovieDetail({
  movieData,
  loveCount,
  userData,
  lovedByUser,
  recommendedMovie,
}) {
  const router = useRouter();
  const { id } = router.query;

  const [loveCountProps, setLoveCountProps] = useState(loveCount);
  const [love, setLove] = useState(lovedByUser);

  if (lovedByUser !== love) setLove(lovedByUser);

  if (loveCountProps !== loveCount) setLoveCountProps(loveCount);

  const handleLoveClick = async () => {
    if (userData) {
      await db.collection("liked_movie").add({
        movie_id: Number(id),
        uid: userData.uid,
      });
      setLove(true);
      setLoveCountProps(loveCountProps + 1);
    } else {
      router.push("/signin");
    }
  };

  const handleUnloveClick = () => {
    const dbGet = async () => {
      return await db
        .collection("liked_movie")
        .where("movie_id", "==", movieData.id)
        .where("uid", "==", `${userData.uid}`)
        .get();
    };
    dbGet().then((snap) => {
      snap.forEach((doc) => {
        doc.ref.delete();
        setLove(false);
        setLoveCountProps(loveCountProps - 1);
      });
    });
  };

  return (
    <BaseLayout user={userData}>
      <Layout className={styles.detailContainer} key={movieData.id}>
        <Layout.Sider className={styles.detailSider} width={`300px`}>
          <Image
            src={`${imagePath}${movieData.poster_path}`}
            preview={false}
            width={"100%"}
          />
        </Layout.Sider>
        <Layout.Content className={styles.detailContent}>
          <div className={styles.loveContainer}>
            <Typography.Text
              className={styles.loveCounterNumber}
            >{`${loveCountProps}`}</Typography.Text>
            {love ? (
              <>
                <HeartFilled
                  className={styles.loveIcon}
                  onClick={handleUnloveClick}
                />
                <div className={styles.lovedContainer}>
                  <Typography.Text
                    className={styles.loved}
                  >{`You loved this`}</Typography.Text>
                </div>
              </>
            ) : (
              <HeartOutlined
                className={styles.loveIcon}
                onClick={handleLoveClick}
              />
            )}
          </div>
          <div className={styles.titleContainer}>
            <Typography.Title className={styles.detailTitle}>
              {movieData.title}
            </Typography.Title>
            <Link href={movieData.homepage}>
              <a target="_blank" rel="noreferrer">
                <LinkOutlined className={styles.linkOfficialWebsite} />
              </a>
            </Link>
          </div>

          <Typography.Text className={styles.detailMovieBrief}>
            ({movieData.original_title}) - Released{" "}
            <Moment format="MMMM Do YYYY">{movieData.release_date}</Moment>
          </Typography.Text>

          {movieData.genres && (
            <div className={styles.genreContainer}>
              <Typography.Text className={styles.genreItem}>
                {movieData.genres.map((genre, index) =>
                  index > 0 ? `, ${genre.name}` : `${genre.name}`
                )}
              </Typography.Text>
            </div>
          )}

          <div className={styles.overviewContainer}>
            <Typography.Text>
              {movieData.overview ? movieData.overview : "No overview."}
            </Typography.Text>
          </div>

          {movieData.production_companies && (
            <div className={styles.productionCompanies}>
              {movieData.production_companies.map(
                (pc) =>
                  pc.logo_path && (
                    <Image
                      key={pc.name}
                      alt={pc.name}
                      className={styles.productionCompanyItem}
                      preview={false}
                      width={50}
                      src={`${imagePathSmall}${pc.logo_path}`}
                    />
                  )
              )}
            </div>
          )}
        </Layout.Content>
      </Layout>
      {recommendedMovie && (
        <Layout className={styles.recommendation}>
          <Layout.Content>
            <Typography.Title>You might also loved this too</Typography.Title>
            <Row gutter={5}>
              {recommendedMovie.map((movie) => (
                <Col span={4} key={movie.id}>
                  <MovieCardMini
                    title={movie.title}
                    id={movie.id}
                    posterPath={movie.poster_path}
                  />
                </Col>
              ))}
            </Row>
          </Layout.Content>
        </Layout>
      )}
    </BaseLayout>
  );
}

export async function getServerSideProps(ctx) {
  const id = ctx.params.id;
  // Fetch data from external API
  const movieData = await getMovieDetail({ id });
  const cookies = nookies.get(ctx);
  let uid = "";
  let userData = null;
  if (cookies.token) {
    const token = await firebaseAdmin.auth().verifyIdToken(cookies.token);
    uid = token.uid;
    userData = {
      ...(await db.collection("users").doc(uid).get()).data(),
      uid: uid,
    };
  }

  const movieIdRef = await db
    .collection("liked_movie")
    .where("movie_id", "==", Number(id))
    .get();
  const loveCount = movieIdRef.size;

  const userLovedRef = db
    .collection("liked_movie")
    .where("movie_id", "==", Number(id))
    .where("uid", "==", `${uid}`)
    .get();
  const lovedByUser = (await userLovedRef).size > 0;

  const recommendedMovie = await getRecommedation({ id });
  const recommendedMovieLimited = recommendedMovie.filter(
    (mov, index) => index < 6
  );

  // Pass data to the page via props
  return {
    props: {
      movieData,
      loveCount,
      userData,
      lovedByUser,
      recommendedMovie: recommendedMovieLimited,
    },
  };
}
