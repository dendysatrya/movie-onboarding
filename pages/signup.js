import Link from "next/link";
import { useRouter } from "next/router";
import { useAuthProvider } from "../utils/AuthProvider";
import { useState } from "react";
import { Button, Input, Form, Card, Layout, Alert } from "antd";
import "antd/dist/antd.css";
import Container from "../components/container";
import {
  MailOutlined,
  LockOutlined,
  UserOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import styles from "../styles/SignUp.module.scss";
import nookies from "nookies";
import firebaseAdmin from "../utils/firebaseAdmin";

const cardHeadStyle = {
  backgroundColor: "black",
  color: "white",
  borderTopLeftRadius: "5px",
  borderTopRightRadius: "5px",
};

const cardBodyStyle = {
  borderBottomLeftRadius: "5px",
  borderBottomRightRadius: "5px",
  boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)",
};

export default function Signup() {
  const [entries, setEntries] = useState({});
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const auth = useAuthProvider();
  const router = useRouter();

  const handleChange = ({ name, value }) => {
    setEntries((prevEntries) => {
      return {
        ...prevEntries,
        [name]: value,
      };
    });
  };

  const submitSignUp = () => {
    if (
      entries.name &&
      entries.email &&
      entries.password &&
      entries.confpassword
    ) {
      if (entries.password === entries.confpassword) {
        setLoading(true);
        auth
          .signUp({
            name: entries.name,
            email: entries.email,
            password: entries.password,
          })
          .then(() => {
            router.push("/");
          });
      } else {
        setError("Password and confirmation password doesn't match.");
      }
    } else {
      setError("Please enter all fields.");
    }
  };

  return (
    <Container>
      {error && (
        <Alert type="error" message={error} className={styles.errorAlert} />
      )}
      <Card
        title="Sign up to MovOn"
        className={styles.card}
        headStyle={cardHeadStyle}
        bodyStyle={cardBodyStyle}
      >
        <Form name="log in" initialValues={{ remember: true }}>
          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                message: "Please enter your email.",
              },
            ]}
          >
            <Input
              className={styles.input}
              name="email"
              size="large"
              placeholder="Email"
              prefix={<MailOutlined />}
              value={entries.email}
              onChange={(e) => handleChange(e.target)}
            />
          </Form.Item>
          <Form.Item
            name="name"
            rules={[
              {
                required: true,
                message: "Please enter your name.",
              },
            ]}
          >
            <Input
              className={styles.input}
              name="name"
              size="large"
              placeholder="Full Name"
              prefix={<UserOutlined />}
              value={entries.name}
              onChange={(e) => handleChange(e.target)}
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Please enter your password.",
              },
            ]}
          >
            <Input.Password
              className={styles.input}
              name="password"
              size="large"
              placeholder="Password"
              prefix={<LockOutlined />}
              value={entries.password}
              onChange={(e) => handleChange(e.target)}
            />
          </Form.Item>
          <Form.Item
            name="confpassword"
            rules={[
              {
                required: true,
                message: "Please enter your password confirmation.",
              },
            ]}
          >
            <Input.Password
              className={styles.input}
              name="confpassword"
              size="large"
              placeholder="Password Confirmation"
              prefix={<LockOutlined />}
              value={entries.confPassword}
              onChange={(e) => handleChange(e.target)}
            />
          </Form.Item>
          <Form.Item className={styles.submitContainer}>
            <Button
              size="large"
              type="primary"
              htmlType="button"
              className={styles.submitButton}
              onClick={submitSignUp}
              disabled={loading}
            >
              {!loading ? (
                "Sign up"
              ) : (
                <>
                  Signing up...&nbsp;
                  <LoadingOutlined style={{ fontSize: "16px" }} />
                </>
              )}
            </Button>
          </Form.Item>
          <Layout className={styles.signInContainer}>
            Already have an account?&nbsp;
            <Link href="/signin">
              <a>Sign in.</a>
            </Link>
          </Layout>
        </Form>
      </Card>
    </Container>
  );
}

export async function getServerSideProps(ctx) {
  const cookies = nookies.get(ctx);
  let uid = null;
  if (cookies.token) {
    const token = await firebaseAdmin.auth().verifyIdToken(cookies.token);
    uid = token.uid;
    ctx.res.writeHead(302, { Location: "/" });
    ctx.res.end();

    return { props: {} };
  }
  return { props: { uid } };
}
