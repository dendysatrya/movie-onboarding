import BaseLayout from "../components/baseLayout";
import firebaseAdmin from "../utils/firebaseAdmin";
import { db } from "../utils/firebaseClient";
import nookies from "nookies";
import { getMovieDetail } from "../utils/fetchQuery";
import { Col, Row, Typography } from "antd";
import MovieCardMini from "../components/movieCardMini";
import Link from "next/link";
import styles from "../styles/MyList.module.scss";

export default function MyList({ userData, movieLovedData }) {
  return (
    <BaseLayout user={userData}>
      <div className={styles.myListContainer}>
        <Typography.Title>My loved movie</Typography.Title>
        <div className={styles.movieListContainer}>
          {movieLovedData ? (
            <Row gutter={10}>
              {movieLovedData.map((movie) => (
                <Col span={4} key={movie.id}>
                  <MovieCardMini
                    title={movie.title}
                    posterPath={movie.posterPath}
                    id={movie.id}
                  />
                </Col>
              ))}
            </Row>
          ) : (
            <Typography.Text>
              It seems you still don't loved any single movie.{" "}
              <Link href="/">
                <a>Let's browse movies</a>
              </Link>
            </Typography.Text>
          )}
        </div>
      </div>
    </BaseLayout>
  );
}

export async function getServerSideProps(ctx) {
  const cookies = nookies.get(ctx);
  let uid = null;
  let userData = null;
  let movieLovedData = [];
  if (cookies.token) {
    const token = await firebaseAdmin.auth().verifyIdToken(cookies.token);
    uid = token.uid;
    userData = { ...(await db.collection("users").doc(uid).get()).data(), uid };

    const movieLoved = await db
      .collection("liked_movie")
      .where("uid", "==", `${uid}`)
      .get();

    movieLovedData = await Promise.all(
      movieLoved.docs.map(async (doc) => {
        const data = doc.data();
        const singleMovie = await getMovieDetail({ id: data.movie_id });
        return {
          id: singleMovie.id,
          title: singleMovie.title,
          posterPath: singleMovie.poster_path,
        };
      })
    );

    return { props: { userData, movieLovedData } };
  } else {
    ctx.res.writeHead(302, { Location: "/" });
    ctx.res.end();

    return { props: {} };
  }
}
