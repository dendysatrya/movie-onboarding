import { Alert, Button, Col, Input, Row, Typography } from "antd";
import { useRouter } from "next/router";
import React, { useState } from "react";
import BaseLayout from "../components/baseLayout";
import nookies from "nookies";
import firebaseAdmin from "../utils/firebaseAdmin";
import { auth, db } from "../utils/firebaseClient";
import { CheckCircleTwoTone, EditOutlined } from "@ant-design/icons";
import { getMovieDetail } from "../utils/fetchQuery";
import MovieCardMini from "../components/movieCardMini";
import styles from "../styles/Profile.module.scss";
import Link from "next/link";

export default function Profile({ userData, movieLovedData }) {
  const router = useRouter();
  const user = auth.currentUser;
  const [changeNameState, setChangeNameState] = useState(false);
  const [newName, setNewName] = useState(userData.name);
  const [changePasswordState, setChangePasswordState] = useState(false);
  const [name, setName] = useState(userData.name);
  const [passwordEntries, setPasswordEntries] = useState({});
  const [changePasswordError, setChangePasswordError] = useState("");
  const [successAlertVisible, setSuccessAlertVisible] = useState(false);
  const [errorAlertVisible, setErrorAlertVisible] = useState(false);
  const [loadingChangePassword, setLoadingChangePassword] = useState(false);

  const handleChangeName = async () => {
    if (!name) {
      return;
    }

    if (name !== newName) {
      await db
        .collection("users")
        .doc(userData.uid)
        .set({ email: userData.email, name: name, role: "user" })
        .then(() => {
          setChangeNameState(false);
          setNewName(name);
        });
    } else {
      setChangeNameState(false);
    }
  };

  const handleChangePassword = (e) => {
    const { name, value } = e.target;
    setPasswordEntries({ ...passwordEntries, [name]: value });
  };

  const handleSubmitChangePassword = async () => {
    if (loadingChangePassword) {
      return;
    }
    if (passwordEntries.pass && passwordEntries.passConf) {
      const samePass = passwordEntries.pass === passwordEntries.passConf;
      if (samePass) {
        setLoadingChangePassword(true);
        await user
          .updatePassword(passwordEntries.pass)
          .then(() => {
            setPasswordEntries({});
            setChangePasswordState(false);
            setSuccessAlertVisible(true);
            setLoadingChangePassword(false);
          })
          .catch(() => {
            setChangePasswordState(false);
            setPasswordEntries({});
            setChangePasswordError(
              "There is problem when we trying to change your password. Please re-login or try again later."
            );
            setErrorAlertVisible(true);
            setLoadingChangePassword(false);
          });
      } else {
        setChangePasswordError(
          "Password & password confirmation didn't match."
        );
        setErrorAlertVisible(true);
      }
    } else {
      setChangePasswordError(
        "Please enter password and password confirmation."
      );
      setErrorAlertVisible(true);
    }
  };

  const handleCloseAlert = (type) => {
    if (type === "success") {
      setSuccessAlertVisible(false);
    } else {
      setErrorAlertVisible(false);
    }
  };

  return (
    <BaseLayout user={{ ...userData, name: newName }}>
      <div className={styles.profileContainer}>
        {successAlertVisible && (
          <Alert
            message="Success change password."
            type="success"
            className={styles.alert}
            closable
            afterClose={() => {
              handleCloseAlert("success");
            }}
          />
        )}
        {errorAlertVisible && (
          <Alert
            message={changePasswordError}
            type="error"
            className={styles.alert}
            closable
            afterClose={() => {
              handleCloseAlert("error");
            }}
          />
        )}
        <Button
          className={styles.changePasswordTopButton}
          disabled={loadingChangePassword}
          onClick={() => {
            setChangePasswordState(!changePasswordState);
            setChangeNameState(false);
            setName(newName);
            setPasswordEntries({});
          }}
        >
          Change Password
        </Button>
        <div className={styles.userProfileInfo}>
          {!changeNameState ? (
            <Typography.Title level={2} className={styles.userNameText}>
              {name}
              &nbsp;
              <EditOutlined
                className={styles.editIcon}
                onClick={() => {
                  setChangeNameState(true);
                  setChangePasswordState(false);
                  setPasswordEntries({});
                }}
              />
            </Typography.Title>
          ) : (
            <div className={styles.changeNameContainer}>
              <Input
                size="large"
                value={name}
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />{" "}
              <CheckCircleTwoTone
                className={styles.changeNameDoneIcon}
                twoToneColor="#52c41a"
                onClick={handleChangeName}
              />{" "}
            </div>
          )}

          <Typography.Text className={styles.emailText}>
            {userData.email}
          </Typography.Text>
        </div>
        <div
          className={styles.changePasswordOuter}
          style={{
            height: changePasswordState ? "140px" : "0",
          }}
        >
          <div className={styles.changePasswordContainer}>
            <Input.Password
              name="pass"
              className={styles.inputPassword}
              placeholder="enter new password"
              size="middle"
              value={passwordEntries.pass}
              disabled={loadingChangePassword}
              onChange={(e) => {
                handleChangePassword(e);
              }}
            />
            <Input.Password
              name="passConf"
              className={styles.inputPassword}
              size="middle"
              value={passwordEntries.passConf}
              disabled={loadingChangePassword}
              onChange={(e) => {
                handleChangePassword(e);
              }}
            />
            <div className={styles.changePasswordButtonContainer}>
              <Button
                className={styles.cancelButton}
                onClick={() => {
                  setChangePasswordState(false);
                  setPasswordEntries({});
                }}
                disabled={loadingChangePassword}
              >
                Cancel
              </Button>
              <Button
                className={styles.submitButton}
                onClick={handleSubmitChangePassword}
                disabled={loadingChangePassword}
              >
                Submit
              </Button>
            </div>
          </div>
        </div>
        <div className={styles.lovedMovie}>
          <Typography.Title level={4}>Movie(s) you loved</Typography.Title>
          <div className={styles.lovedMovieListContainer}>
            {movieLovedData.length ? (
              <Row gutter={5}>
                {movieLovedData.map((movie, index) =>
                  index === 7 ? (
                    <Col span={6}>
                      <Link href="/my-list">
                        <a>
                          <div className={styles.seeMorePlaceholder}>
                            <Typography.Text className={styles.seeMoreText}>
                              See more...
                            </Typography.Text>
                          </div>
                        </a>
                      </Link>
                    </Col>
                  ) : (
                    <Col span={6} key={movie.id}>
                      <MovieCardMini
                        title={movie.title}
                        id={movie.id}
                        posterPath={movie.posterPath}
                      />
                    </Col>
                  )
                )}
              </Row>
            ) : (
              <Typography.Text className={styles.noMovie}>
                No movie loved
              </Typography.Text>
            )}
          </div>
        </div>
      </div>
    </BaseLayout>
  );
}

export async function getServerSideProps(ctx) {
  const cookies = nookies.get(ctx);

  let uid = "";
  let userData = null;
  let movieLovedData = [];
  if (cookies.token) {
    const token = await firebaseAdmin.auth().verifyIdToken(cookies.token);
    uid = token.uid;
    userData = { ...(await db.collection("users").doc(uid).get()).data(), uid };

    const movieLoved = await db
      .collection("liked_movie")
      .where("uid", "==", `${uid}`)
      .limit(8)
      .get();

    movieLovedData = await Promise.all(
      movieLoved.docs.map(async (doc) => {
        const data = doc.data();
        const singleMovie = await getMovieDetail({ id: data.movie_id });
        return {
          id: singleMovie.id,
          title: singleMovie.title,
          posterPath: singleMovie.poster_path,
        };
      })
    );

    return { props: { userData, movieLovedData } };
  } else {
    ctx.res.writeHead(302, { Location: "/signin" });
    ctx.res.end();

    return { props: {} };
  }
}
