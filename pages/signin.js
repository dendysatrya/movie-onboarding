import Link from "next/link";
import { useRouter } from "next/router";
import { Button, Input, Form, Card, Layout, Alert } from "antd";
import "antd/dist/antd.css";
import Container from "../components/container";
import { MailOutlined, LockOutlined, LoadingOutlined } from "@ant-design/icons";
import { useAuthProvider } from "../utils/AuthProvider";
import { useEffect, useState } from "react";
import nookies from "nookies";
import firebaseAdmin from "../utils/firebaseAdmin";
import { auth } from "../utils/firebaseClient";
import styles from "../styles/SignIn.module.scss";

const cardHeadStyle = {
  backgroundColor: "black",
  color: "white",
  borderTopLeftRadius: "5px",
  borderTopRightRadius: "5px",
};

const cardBodyStyle = {
  borderBottomLeftRadius: "5px",
  borderBottomRightRadius: "5px",
  boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)",
};

export default function SignIn() {
  const router = useRouter();
  const user = auth.currentUser;
  const useAuth = useAuthProvider();
  const [entries, setEntries] = useState({});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");

  const handleChange = ({ name, value }) => {
    setEntries((prevEntries) => {
      return {
        ...prevEntries,
        [name]: value,
      };
    });
  };

  const signInClick = async () => {
    setLoading(true);
    await useAuth
      .signIn(entries)
      .then((res) => {
        if (res) {
          router.push("/");
        }
      })
      .catch((err) => {
        setLoading(false);
        setError("Invalid email or password.");
      });
  };

  return (
    <Container>
      {error && (
        <Alert type="error" message={error} className={styles.errorAlert} />
      )}
      <Card
        title="Sign in into MovOn"
        className={styles.card}
        headStyle={cardHeadStyle}
        bodyStyle={cardBodyStyle}
      >
        <Form name="log in" initialValues={{ remember: true }}>
          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                message: "Please enter your email.",
              },
            ]}
          >
            <Input
              name="email"
              size="large"
              placeholder="Email"
              value={entries.email}
              prefix={<MailOutlined />}
              onChange={(event) => {
                handleChange(event.target);
              }}
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Please enter your password.",
              },
            ]}
          >
            <Input.Password
              name="password"
              size="large"
              placeholder="Password"
              value={entries.password}
              prefix={<LockOutlined />}
              onChange={(event) => {
                handleChange(event.target);
              }}
            />
          </Form.Item>
          <Form.Item className={styles.submitContainer}>
            <Button
              size="large"
              type="primary"
              htmlType="submit"
              className={styles.submitButton}
              onClick={signInClick}
              disabled={loading}
            >
              {!loading ? (
                "Sign In"
              ) : (
                <>
                  Signing in...&nbsp;
                  <LoadingOutlined style={{ fontSize: "16px" }} />
                </>
              )}
            </Button>
          </Form.Item>
          <Layout className={styles.signUpContainer}>
            Don't have account?&nbsp;
            <Link href="/signup">
              <a>Sign up here.</a>
            </Link>
          </Layout>
        </Form>
      </Card>
    </Container>
  );
}

export async function getServerSideProps(ctx) {
  const cookies = nookies.get(ctx);
  let uid = null;
  if (cookies.token) {
    const token = await firebaseAdmin.auth().verifyIdToken(cookies.token);
    uid = token.uid;
    ctx.res.writeHead(302, { Location: "/" });
    ctx.res.end();

    return { props: {} };
  }
  return { props: {} };
}
