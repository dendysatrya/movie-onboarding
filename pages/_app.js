import "../styles/globals.scss";
import { AuthProvider } from "../utils/AuthProvider";
import Router from "next/router";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import "antd/dist/antd.css";

function MyApp({ Component, pageProps }) {
  NProgress.configure({ showSpinner: false });

  Router.onRouteChangeStart = () => {
    NProgress.start();
  };

  Router.onRouteChangeComplete = () => {
    NProgress.done();
  };

  Router.onRouteChangeError = () => {
    NProgress.done();
  };

  return (
    <AuthProvider>
      <Component {...pageProps} />
    </AuthProvider>
  );
}

export default MyApp;
