import { useRouter } from "next/router";
import { useEffect } from "react";
import { useAuthProvider } from "../utils/AuthProvider";

export default function SignOut() {
  const router = useRouter();
  const auth = useAuthProvider();
  useEffect(() => {
    const signOutProcess = async () => {
      const signOut = await auth.signOut();
      router.push("/signin");
    };

    signOutProcess();
  }, []);

  return null;
}
