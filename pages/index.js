import { useRouter } from "next/router";
import BaseLayout from "../components/baseLayout";
import { Col, Row } from "antd";
import { discoverMovie } from "../utils/fetchQuery";
import MovieCard from "../components/movieCard";
import ReactPaginate from "react-paginate";
import { db } from "../utils/firebaseClient";
import nookies from "nookies";
import firebaseAdmin from "../utils/firebaseAdmin";
import styles from "../styles/Pagination.module.scss";

export default function Home({ movieData, totalPages, currentPage, userData }) {
  const route = useRouter();

  const handlePagination = (page) => {
    const currentPath = route.pathname;
    const currentQuery = route.query;
    currentQuery.page = page.selected + 1;

    if (currentQuery.page !== 1) {
      route.push({
        pathname: currentPath,
        query: currentQuery,
      });
    } else {
      route.push({
        pathname: currentPath,
      });
    }
  };

  return (
    <BaseLayout user={userData}>
      <Row gutter={[24]}>
        {movieData &&
          movieData.map((movie) => (
            <Col span={6} key={movie.title}>
              <MovieCard
                id={movie.id}
                title={movie.title}
                posterImage={movie.poster_path}
                releaseDate={movie.release_date}
                loveCount={movie.loveCount}
                loved={movie.lovedByUser}
              />
            </Col>
          ))}
      </Row>
      <ReactPaginate
        previousLabel={"<"}
        nextLabel={">"}
        breakLabel={"..."}
        breakClassName={"pagination-break"}
        activeClassName={styles.active}
        containerClassName={styles.pagination}
        nextClassName={styles.next}
        previousClassName={styles.previous}
        subContainerClassName={"pages pagination"}
        initialPage={currentPage - 1}
        pageCount={totalPages}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={handlePagination}
      />
    </BaseLayout>
  );
}

export async function getServerSideProps(ctx) {
  const page = ctx.query.page || 1;
  const rawData = await discoverMovie({ params: page });
  const movieData = rawData.results;
  const totalPages = rawData.total_pages;
  const cookies = await nookies.get(ctx);
  let uid = "";
  let userData = null;
  if (cookies.token) {
    const token = await firebaseAdmin.auth().verifyIdToken(cookies.token);
    uid = await token.uid;
    userData = (await db.collection("users").doc(uid).get()).data();
  }

  let movieWithAllState = [];
  movieWithAllState = await Promise.all(
    movieData.map(async (movie) => {
      const loveCountRef = db
        .collection("liked_movie")
        .where("movie_id", "==", Number(movie.id))
        .get();
      const userLovedRef = db
        .collection("liked_movie")
        .where("movie_id", "==", Number(movie.id))
        .where("uid", "==", `${uid}`)
        .get();
      const boolResult = (await userLovedRef).size > 0;
      return {
        ...movie,
        loveCount: (await loveCountRef).size,
        lovedByUser: boolResult,
      };
    })
  );

  // Pass data to the page via props
  return {
    props: {
      movieData: movieWithAllState,
      totalPages,
      currentPage: page,
      userData,
    },
  };
}
