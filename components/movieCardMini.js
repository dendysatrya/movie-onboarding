import { Card, Image, Typography } from "antd";
import Link from "next/link";
import { imagePath } from "../constants/tmbdConsts";
import styles from "../styles/MovieCardMini.module.scss";

export default function MovieCardMini({ title, id, posterPath }) {
  return (
    <Card
      bordered={false}
      className={styles.movieCardMini}
      bodyStyle={{ padding: 0 }}
    >
      <Link href={`/movie/${id}`}>
        <a>
          <Image
            src={`${imagePath}${posterPath}`}
            preview={false}
            width={"100%"}
          />
          <Typography.Paragraph
            className={styles.movieTitle}
            ellipsis={{ rows: 2 }}
          >
            {title}
          </Typography.Paragraph>
        </a>
      </Link>
    </Card>
  );
}
