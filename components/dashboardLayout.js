import Layout, { Content, Footer } from "antd/lib/layout/layout";
import Header from "./header";
import styles from "../styles/DashboardLayout.module.scss";
import Sider from "antd/lib/layout/Sider";
import { CaretRightFilled } from "@ant-design/icons";
import { Card } from "antd";

export default function DashboardLayout({ children, user = null }) {
  return (
    <Layout className={styles.layout}>
      <Header user={user} dashboard={true} />
      <Layout className={styles.layoutInner}>
        <Sider className={styles.sider}>
          <ul className={styles.menuParent}>
            <li className={styles.menuItem}>
              Users <CaretRightFilled className={styles.rightArrow} />
            </li>
          </ul>
        </Sider>
        <Content className={styles.container}>
          <Card className={styles.cardContent} bodyStyle={{ padding: 0 }}>
            {children}
          </Card>
        </Content>
      </Layout>
    </Layout>
  );
}
