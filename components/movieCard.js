import React, { useEffect, useState } from "react";
import { Card, Image, Typography, Tooltip } from "antd";
import { imagePath } from "../constants/tmbdConsts";
import { HeartFilled, HeartOutlined } from "@ant-design/icons";
import { useAuth } from "../utils/AuthProvider";
import { db } from "../utils/firebaseClient";
import { useRouter } from "next/router";
import styles from "../styles/MovieCard.module.scss";

export default function MovieCard({
  title,
  posterImage,
  id,
  loveCount,
  loved,
}) {
  const [love, setLove] = useState(loved);
  const [loveCountProps, setLoveCountProps] = useState(loveCount);
  const user = useAuth().user;
  const router = useRouter();

  const handleLoveClick = async () => {
    if (user) {
      await db.collection("liked_movie").add({
        movie_id: id,
        uid: user.uid,
      });
      setLove(true);
      setLoveCountProps(loveCountProps + 1);
    } else {
      router.push("/signin");
    }
  };

  const handleUnloveClick = () => {
    const dbGet = async () => {
      return await db
        .collection("liked_movie")
        .where("movie_id", "==", id)
        .where("uid", "==", `${user.uid}`)
        .get();
    };
    dbGet().then((snap) => {
      snap.forEach((doc) => {
        doc.ref.delete();
        setLove(false);
        setLoveCountProps(loveCountProps - 1);
      });
    });
  };

  const handleClickMovie = () => {
    router.push(`/movie/${id}`);
  };

  return (
    <Card
      className={styles.antCard}
      bordered={false}
      bodyStyle={{ padding: 0 }}
    >
      <Image
        src={`${imagePath}${posterImage}`}
        preview={false}
        width={"100%"}
      />
      <div className={styles.preview}>
        <div className={styles.love}>
          <div className={styles.loveCount}>{loveCountProps}</div>
          {love ? (
            <HeartFilled
              className={styles.anticonHeart}
              onClick={() => handleUnloveClick()}
            />
          ) : (
            <HeartOutlined
              className={styles.anticonHeart}
              onClick={() => handleLoveClick()}
            />
          )}
        </div>
        <div className={styles.movieName}>
          <Typography.Title
            onClick={handleClickMovie}
            className={styles.movieNameText}
            level={1}
            ellipsis={{ rows: 2 }}
          >
            {title}
          </Typography.Title>
        </div>
      </div>
    </Card>
  );
}
