import { Layout } from "antd";
import styles from "../styles/Container.module.scss";

export default function Container({ children }) {
  return <Layout className={styles.container}>{children}</Layout>;
}
