import Layout, { Content, Footer } from "antd/lib/layout/layout";
import Header from "./header";
import styles from "../styles/BaseLayout.module.scss";

export default function BaseLayout({ children, user = null }) {
  return (
    <Layout>
      <Header user={user} />
      <Layout className={styles.layout}>
        <Content className={styles.container}>{children}</Content>
      </Layout>
    </Layout>
  );
}
