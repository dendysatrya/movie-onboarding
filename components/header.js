import React from "react";
import { Header } from "antd/lib/layout/layout";
import Link from "next/link";
import {
  HeartOutlined,
  LogoutOutlined,
  SettingOutlined,
  SolutionOutlined,
} from "@ant-design/icons";
import { useRouter } from "next/router";
import styles from "../styles/Header.module.scss";
import { Typography } from "antd";

export default function BaseHeader({ user, dashboard = false }) {
  const router = useRouter();

  return (
    <Header className={styles.header}>
      <div className={styles.headerChildren}>
        <div className={styles.headerBrand}>
          <Link href="/">
            {dashboard ? (
              <a>
                <div className={styles.dashboardBrand}>
                  <Typography.Text className={styles.movOnBrand}>
                    MovOn
                  </Typography.Text>
                  <Typography.Text className={styles.dashboardText}>
                    Dashboard
                  </Typography.Text>
                </div>
              </a>
            ) : (
              <a>MovOn</a>
            )}
          </Link>
        </div>
      </div>
      <div className={styles.headerChildren}>
        {user ? (
          <div className={styles.headerUser}>
            {user.name ? user.name : user.email}
            <div className={styles.headerUserMenu}>
              <ul>
                {user.role === "admin" && (
                  <Link href="/dashboard">
                    <a>
                      <li>
                        <SettingOutlined /> Dashboard
                      </li>
                    </a>
                  </Link>
                )}
                <Link href="/profile">
                  <a>
                    <li>
                      <SolutionOutlined /> Profile
                    </li>
                  </a>
                </Link>
                <Link href="/my-list">
                  <a>
                    <li>
                      <HeartOutlined /> Loved Movie
                    </li>
                  </a>
                </Link>
                <Link href="/signout">
                  <a>
                    <li>
                      <LogoutOutlined /> Sign Out
                    </li>
                  </a>
                </Link>
              </ul>
            </div>
          </div>
        ) : (
          <div className={styles.headerChildrenItem}>
            <Link href="/signin">
              <a className={styles.buttonSignIn}>Sign In</a>
            </Link>
          </div>
        )}
      </div>
    </Header>
  );
}
