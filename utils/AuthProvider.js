import { createContext, useContext, useEffect, useState } from "react";
import { app, auth, db } from "./firebaseClient";
import nookies from "nookies";

export const AuthContext = createContext(auth.currentUser || null);

export function AuthProvider({ children }) {
  const [user, setUser] = useState(auth.currentUser || null);

  useEffect(() => {
    return auth.onIdTokenChanged(async (user) => {
      if (user) {
        const token = await user.getIdToken();
        const userData = (
          await db.collection("users").doc(user.uid).get()
        ).data();
        setUser({ ...userData, uid: user.uid });
        nookies.destroy(null, "token");
        nookies.set(null, "token", token);
      } else {
        setUser(null);
        nookies.destroy(null, "token");
        nookies.set(null, "token", "", {});
      }
    });
  }, []);

  //refresh token every 10 minutes
  useEffect(() => {
    const handle = setInterval(async () => {
      const user = auth.currentUser;
      if (user) await user.getIdToken(true);
    }, 10 * 60 * 1000);

    // clean up setInterval
    return () => clearInterval(handle);
  }, []);

  return (
    <AuthContext.Provider value={{ user }}>{children}</AuthContext.Provider>
  );
}

export const useAuth = () => {
  return useContext(AuthContext);
};

export const useAuthProvider = () => {
  const [user, setUser] = useState(null);
  const createUser = (user) => {
    return db
      .collection("users")
      .doc(user.uid)
      .set({ email: user.email, name: user.name, role: "user" })
      .then(() => {
        return user;
      })
      .catch((error) => {
        console.log(error);
        return { error };
      });
  };

  const signUp = ({ name, email, password }) => {
    return auth
      .createUserWithEmailAndPassword(email, password)
      .then((response) => {
        //auth.currentUser.sendEmailVerification();
        return createUser({ uid: response.user.uid, email, name });
      })
      .catch((error) => {
        return { error };
      });
  };

  const signIn = ({ email, password }) => {
    return auth
      .signInWithEmailAndPassword(email, password)
      .then(async (response) => {
        setUser(response);
        const userSigned = await auth.currentUser;
        const token = await userSigned.getIdToken();
        nookies.destroy(null, "token");
        nookies.set(null, "token", token);
        return userSigned;
      });
  };

  const signOut = () => {
    return auth.signOut().then(() => {
      nookies.destroy(null, "token");
      nookies.set(null, "token", "", {});
      setUser(null);
    });
  };

  return { user, signUp, signIn, signOut };
};
