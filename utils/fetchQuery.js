import react from "react";
import {
  baseUrl,
  authApi,
  discoverUrl,
  recommendation,
  movieUrl,
  sortByReleaseDate,
  pagination,
} from "../constants/tmbdConsts";

async function discoverMovie({ params }) {
  const res = await (
    await fetch(
      `${baseUrl}${discoverUrl}${authApi}${sortByReleaseDate}${pagination}${params}`
    )
  ).json();
  const data = res;
  return data;
}

async function getMovieDetail({ id }) {
  const res = await (
    await fetch(`${baseUrl}${movieUrl}${id}${authApi}`)
  ).json();
  const data = res;
  return data;
}

async function getRecommedation({ id }) {
  const res = await (
    await fetch(`${baseUrl}${movieUrl}${id}${recommendation}${authApi}`)
  ).json();
  const data = res.results;
  return data;
}

export { discoverMovie, getMovieDetail, getRecommedation };
