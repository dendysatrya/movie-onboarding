import firebase from "firebase";

import fsas from "../fsas.json";

export const firebaseConfig = {
  apiKey: fsas.base.apiKey,
  authDomain: fsas.base.authDomain,
  databaseURL: fsas.base.databaseURL,
  projectId: fsas.base.projectId,
  storageBucket: fsas.base.storageBucket,
  messagingSenderId: fsas.base.messagingSenderId,
  appId: fsas.base.appId,
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}
const app = firebase.app();
const auth = firebase.auth();
const db = firebase.firestore();
const now = firebase.firestore.Timestamp.now();

export { app, auth, db, now };
