import firebaseAdmin from "firebase-admin";
import fasas from "../fasas.json";
import fsas from "../fsas.json";

if (!firebaseAdmin.apps.length) {
  firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert({
      privateKey: fasas.private_key,
      clientEmail: fasas.client_email,
      projectId: fasas.project_id,
    }),
    databaseURL: fsas.base.databaseURL,
  });
}

export default firebaseAdmin;
